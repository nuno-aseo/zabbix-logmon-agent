import argparse
import logging
import importlib
from pygtail import Pygtail
import os
import sys
import mappings
import yaml
import time
import socket
from zbxsend import send_to_zabbix

sys.path.append(os.path.realpath(os.path.dirname(__file__)))

LOG_FORMAT = '%(levelname) -10s %(asctime)s %(process) -10s %(funcName) -20s %(lineno) -5d: %(message)s'


class App(object):
    def __init__(self):
        self.log = '/var/log/messages'
        self.config = './config.yml'
        self.parser_group = ''
        self.registered_parsers = {}
        self.client_host = ''
        self.zabbix_host = ''
        self.zabbix_port = ''
        self.aggregations = {}

    def get_parsers_group(self, file, group):
        f = open(file, 'r')
        data = yaml.load(f)
        logging.info('loading config from %s', file)
        logging.debug('config:')
        logging.debug(data)

        return data[group]

    def get_parsers_for_group(self, group):
        parser_data = group['parsers']
        ret = []

        for parser_id in parser_data:
            parser_config = parser_data[parser_id]
            obj = self.get_parser_object(parser_config['parser'])
            obj.configure(parser_config)

            ret.append(obj)

        return ret

    def get_parser_object(self, parser_id):
        parser_class_raw = mappings.parsers[parser_id]
        parts = parser_class_raw.split('.')

        cls = parts.pop()
        mdl = ".".join(parts)

        parser = self.str_to_class(mdl, cls)

        return parser

    def str_to_class(self, module_name, class_name):
        try:
            class_ = None
            module_ = importlib.import_module(module_name)
            try:
                class_ = getattr(module_, class_name)()
            except AttributeError:
                logging.error('Class does not exist')
        except ImportError:
            logging.error('Module %s does not exist', module_name)
        return class_ or None

    def monitor_file(self, parsers, file):
        for line in Pygtail(file):
            logging.debug("---------------------")
            logging.debug('processing line: %s', line)
            for parser in parsers:
                item = parser.parse(line)
                if item:
                    logging.debug("parser %s matched line", parser.id)
                    logging.debug("\taggregation threshold: %s", parser.aggregate)
                    logging.debug("\taggregation level: %s", len(parser.data))
                    if parser.aggregate == len(parser.data):
                        item = parser.get_event()

                        item.host = self.client_host
                        logging.info('about to send data to zabbix: %s', item)
                        send_to_zabbix([item], self.zabbix_host, self.zabbix_port)

                    break
                logging.debug("parser %s did not match", parser.id)

    def monitor(self):

        parser_group = self.get_parsers_group(self.config, self.parser_group)
        parsers = self.get_parsers_for_group(parser_group)

        while True:
            self.monitor_file(parsers, self.log)
            time.sleep(1)


def main():
    '''
        Command-line parameters and decoding for Zabbix use/consumption.
    '''

    parser = argparse.ArgumentParser(description='Log monitor Help')
    parser.add_argument('--log', help='log file to monitor', default='/var/log/messages')
    parser.add_argument('--config', help='parser configuration file', default='./monitor.yml')
    parser.add_argument('--parser', help='parser configuration to execute')
    parser.add_argument('--client-host',  help='the hostname to report to zabbix',     default=socket.gethostname())
    parser.add_argument('--zbx-host',     help='Zabbix API host',        default='localhost')
    parser.add_argument('--zbx-port',     help='Zabbix API port',        default=10051, type=int)
    parser.add_argument('--logging',      help='the logging level',      default='error')

    options = parser.parse_args()

    log_lvl = logging.ERROR
    if 'debug' == options.logging.lower():
        log_lvl = logging.DEBUG
    if 'info' == options.logging.lower():
        log_lvl = logging.INFO
    if 'warning' == options.logging.lower():
        log_lvl = logging.WARNING
    if 'error' == options.logging.lower():
        log_lvl = logging.ERROR

    logging.basicConfig(level=log_lvl, format=LOG_FORMAT)

    app = App()
    app.log = options.log
    app.config = options.config
    app.parser_group = options.parser
    app.client_host = options.client_host
    app.zabbix_host = options.zbx_host
    app.zabbix_port = options.zbx_port

    app.monitor()

if __name__ == '__main__':
    main()
