import libs.abstracts
import logging


class GrepAndExtractParser(libs.abstracts.Parser):
    def __init__(self):
        self.id = "grep.extract"
        self.key = ''
        self.grep = ''
        self.extract = 0
        self.separator = None
        self.aggregate = 1
        self.aggregate_type = 'average'
        self.data = []

    def get_event(self):


        value = None
        if 'count' == self.aggregate_type:
            value = len(self.data)

        if 'average' == self.aggregate_type:
            value = reduce(lambda x, y: float(x) + float(y), self.data) / len(self.data)

        if 'none' == self.aggregate_type:
            value = self.data[0]


        self.data = []

        return libs.abstracts.Event(key=self.key, value=value)

    def parse(self, line):

        if not isinstance(self.grep, list):
            self.grep = [self.grep]

        for grep in self.grep:
            if line.find(grep) < 0:
                logging.debug("%s not in %s", grep, line)
                return None

        parts = line.split(self.separator)

        try:
            value = parts[self.extract]
        except Exception as e:
            logging.debug(e)
            return False

        self.data.append(value)

        return True
