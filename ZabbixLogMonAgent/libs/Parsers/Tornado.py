import libs.abstracts
import string
import logging


class TornadoResponseTime(libs.abstracts.Parser):
    def __init__(self):
        super(TornadoResponseTime, self).__init__()

        self.id = "tornado.response.time"
        self.key = 'http.response.time'
        self.interesting_response_codes = range(1, 600)
        self.interesting_methods = ['get', 'post', 'put', 'delete']
        self.separator = None

        self.code_position = 6
        self.method_position = 7
        self.time_position = 10

        self.method_in_key = True
        self.status_in_key = True

        self.data = []
        self.aggregate = 1

    def get_event(self):

        value = reduce(lambda x, y: float(x) + float(y), self.data) / len(self.data)
        self.data = []

        key = self.key

        return libs.abstracts.Event(key=key, value=value)

    def parse(self, line):
        parts = line.split(self.separator)

        try:
            code = int(parts[self.code_position])
            method = parts[self.method_position].lower()
            time_str = parts[self.time_position]
        except Exception as e:
            logging.debug('%s failed to match line: %s', self.id, str(e))
            return False

        t = string.maketrans('', '')
        time = time_str.translate(t, string.letters)

        logging.debug('%s :: is code %s in %s? %s', self.id, code, self.interesting_response_codes, code in self.interesting_response_codes)
        logging.debug('%s :: is method %s in %s? %s', self.id, method, self.interesting_methods, method in self.interesting_methods)

        if code in self.interesting_response_codes and method in self.interesting_methods:
            self.data.append(time)
            return True

        return False

class TornadoResponseCount(TornadoResponseTime):
    def __init__(self):
        super(TornadoResponseCount, self).__init__()

        self.id = "tornado.response"
        self.key = 'http.response.count'

    def get_event(self):

        value = len(self.data)
        self.data = []

        key = self.key

        return libs.abstracts.Event(key=key, value=value)
