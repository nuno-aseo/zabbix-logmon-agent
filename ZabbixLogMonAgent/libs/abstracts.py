import logging


class Event(object):
    def __init__(self, host='', key='', value='', clock=None):
        self.host = host
        self.key = key
        self.value = value
        self.clock = clock

    def __repr__(self):
        if self.clock is None:
            return 'Event(%r, %r, %r)' % (self.host, self.key, self.value)

        return 'Event(%r, %r, %r, %r)' % (self.host, self.key, self.value, self.clock)


class Parser(object):
    def parse(self, line):
        raise NotImplementedError()

    def get_event(self):
        raise NotImplementedError()

    def configure(self, data):
        # keys from the config that the parser does not use
        # for example the parser keys
        keys_to_ignore = ['parser']

        for key in data.keys():
            if key in keys_to_ignore:
                continue

            if key not in self.__dict__:
                raise AttributeError('%s:: %s not a valid key' % (self.id, key))

            setattr(self, key, data[key])
            # self[key] = data[key]
            logging.debug('%s:: set %s to %s', self.id, key, getattr(self, key))
