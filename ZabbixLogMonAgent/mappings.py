parsers = {
    'tornado.response.time': 'libs.Parsers.Tornado.TornadoResponseTime',
    'tornado.response.count': 'libs.Parsers.Tornado.TornadoResponseCount',
    'grep.extract': 'libs.Parsers.Generic.GrepAndExtractParser',
}
