from setuptools import setup, find_packages
setup(
    name="zabbix-logmon-agent",

    author="Nuno Franco da Costa",

    author_email="nuno@francodacosta.com",

    description="collects important stats from log files",

    version="0.2.1",

    url="https://bitbucket.org/francodacosta/zabbix-logmon-agent",

    packages=find_packages(),

    entry_points={
        'console_scripts': [
            'zabbix-logmon-agent = ZabbixLogMonAgent.monitor:main',
        ]
    },

    install_requires=[
        "zbxsend",
        "pygtail",
        "pyaml",
        "zbxsend",
    ]
)
